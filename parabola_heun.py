"""
2022/02/20
球の投げ上げ運動@２次元
ホイン法 Heun Methoe.
"""
from cProfile import label
import numpy as np
import matplotlib.pyplot as plt

# 定数
#g = 9.80665  # 重力加速度
g = 10.0  # 重力加速度

t = 0.0  # 時刻t
dt = 0.05  # 時刻の刻み幅

# 初期条件
theta = (np.pi)/4
V = (2)**0.5


## 運動の計算_前進オイラー法
u_ = V * np.cos(theta)
v_ = V * np.sin(theta)
x_ = 0
y_ = 0

x_list = [x_]
y_list = [y_]

while y_ >= 0:
     x_ += dt * u_
     u_ += dt * 0
     y_ += dt * v_
     v_ += dt * (-g)
     #print("{:.3} {:.3}".format(x, y))
     # グラフデータに現在位置を追加
     if y_ >= 0:
         x_list.append(x_)
         y_list.append(y_) 



## 運動の計算_ホイン法
u = V * np.cos(theta)
v = V * np.sin(theta)
x = 0
y = 0

xlist = [x]
ylist = [y]

# 運動の計算
while y >= 0:
    t += dt
    x += dt * u
    u += dt * 0
    y += dt * 0.5 * (2*v - g*dt)
    v += dt * (-g)
    print("{:.3} {:.3}".format(x, y))
    # グラフデータに現在位置を追加
    if y >= 0:
        xlist.append(x)
        ylist.append(y)

# グラフの表示
plt.title("Methods, dt=0.05")
plt.xlabel("x axis")
plt.ylabel("y axis")

# 解析値の描画
xa = np.arange(0, 0.21, 0.01)
ya = -5.0*xa*(xa - 0.2)

plt.plot(xa, ya,label="Analysis", color="black", lw=6)
plt.plot(x_list, y_list, label="Euler Method", marker="o", color="red")
plt.plot(xlist, ylist, label="Heun Method", marker="o", color="c")
plt.legend()  #グラフの汎用ラベルの表示に必要。
plt.grid()
plt.show()
