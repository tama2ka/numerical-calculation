"""
2022/02/12
自由落下のシミュレーション
自由落下の運動方程式を数値的に解く
前進オイラー法の計算スキーム
"""
import numpy as np
import matplotlib.pyplot as plt

### 定数
g = 9.80665 # 重力加速度

### メイン実行部
t = 0.0    # 時刻t
dt = 0.01  # 時刻の刻み幅

### 係数の入力
#v = float(input("初速度 v0 を入力してください。:"))
#y = float(input("初期高度y0を入力してくだい。"))
v = 3
y = 10
#print("{:.7f} {:.7f} {:.7f}".format(t, y, v))

# グラフデータに現在位置を追加
tlist = [t]
ylist = [y]

### 自由落下の計算
while y >= 0:
    t += dt
    y = y + v * dt    # １つ前の時刻の速度vを使用 
    v = v + (-g) * dt # １つ前の時刻の速度vを使用 

            
    #print("{:.7f} {:.7f} {:.7f}".format(t, y, v))
    print("{:.7f} {:.7f}".format(t, y,))
    # グラフデータに現在位置を追加
    tlist.append(t)
    ylist.append(y)

# グラフの表示
plt.plot(tlist, ylist)
plt.show()
### freefall.py終了


