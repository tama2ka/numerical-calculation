"""
2022/02/13
球の投げ上げ運動@２次元
前進オイラー法
"""
import math
import matplotlib.pyplot as plt
# 定数
g = 9.80665  # 重力加速度


### メイン実行部
t = 0.0  # 時刻t
dt = 0.01  # 時刻の刻み幅

# 初期条件
theta = (math.pi)/4
V = 5
u = V * math.cos(theta)
v = V * math.sin(theta)
x = 0
y = 0

xlist = [x]
ylist = [y]

# 運動の計算
while y >= 0:
    t += dt
    x += dt * u
    u += dt * 0
    y += dt * v
    v += dt * (-g)
    print("{:.3} {:.3}".format(x, y))
    # グラフデータに現在位置を追加
    xlist.append(x)
    ylist.append(y)

# グラフの表示
plt.plot(xlist, ylist)
plt.show()
